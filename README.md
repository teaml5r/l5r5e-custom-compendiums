# Legend of the Five Rings (5th Edition) - Custom Compendium Template
![Banner Legend of the Five Rings](./l5rBan.jpg)

[![Buy Me a Coffee](https://img.shields.io/badge/Buy%20Me%20a-☕%20Coffee-red)](https://ko-fi.com/vlyan)
[![System version](https://img.shields.io/badge/L5R5e-v1.9.x+-green)](https://foundryvtt.com/packages/l5r5e)
[![FoundryVTT version](https://img.shields.io/badge/FVTT-v10/v11.x-informational)](https://foundryvtt.com/)

This compendium module is for the game system [Legend of the Five Rings 5th edition](https://gitlab.com/teaml5r/l5r5e) for [Foundry Virtual Tabletop (FoundryVTT)](https://foundryvtt.com/).

This compendium is an exact copy of the system's english compendiums with Babele structure.
The idea is to allow you to fill descriptions, or translate in another language, without breaking associations (properties, school cursus...).


## Legal
Do not share this compendium filled with book descriptions ! This is only allowed for private use.


## Install (no manifest)
As you will spend a lot of time filling this compendium, i don't want some update to remove all your work, so no automatic update !
+ Click on the "[Download](https://gitlab.com/teaml5r/l5r5e-custom-compendiums/-/archive/master/l5r5e-custom-compendiums-master.zip)" button
+ Extract the archive.
+ Open the `l5r5e-custom-compendiums-master` folder.
+ Copy/Move the `l5r5e-custom-compendiums` folder in your foundry `modules` data folder.
+ The final result must be something like `<FoundryData>/modules/l5r5e-custom-compendiums/register-babele.js`.

Note : Keep the module name to `l5r5e-custom-compendiums` as i need to disable embed translation in system if this module exists.


## Requirements
- L5R5e: Search in `Game Systems` tab and download it directly from FoundryVTT (or from : https://foundryvtt.com/packages/l5r5e)
- Babele: Search in `Add-ons Modules` tab and download it directly from FoundryVTT (or from : https://foundryvtt.com/packages/babele)


## Change the target language
- Edit `register-babele.js` with some text editor (Notepad++, Visual Studio Code...) and change the corresponding language :
  - `lang: 'en',`  (ex change to `fr`)
  - `dir: 'compendiums/en'` (ex change to `compendiums/fr`)
- Rename the folder in `compendiums` according to the previous edit (`dir`), ex : `fr`).


## Adding more languages
- Edit `register-babele.js` and add the corresponding language section.

Full example with EN and FR :
```js
Hooks.once('init', () => {
    if (typeof Babele !== 'undefined') {
        // En
        Babele.get().register({
            module: 'l5r5e-custom-compendiums',
            lang: 'en',
            dir: 'compendiums/en'
        });

        // Fr
        Babele.get().register({
            module: 'l5r5e-custom-compendiums',
            lang: 'fr',
            dir: 'compendiums/fr'
        });
    }
});
```


## Adding descriptions

### Custom Compendium Maker (highly recommended)
I encourage you to use the [Custom Compendium Maker](https://vly.yn.lu/l5r5e-custom-compendium-maker/).

This tool allow you to fill a Excel template to generate all the Babele's files for one language at time.

Simply follow the instructions in the website, and extract all the result files in `l5r5e-custom-compendiums/compendiums/`.

Edit the `l5r5e-custom-compendiums/register-babele.js` file accordingly to add or modify the used languages declarations.

You also can check the [wiki page](https://gitlab.com/teaml5r/l5r5e/-/wikis/users/custom-compendiums.md) for more description.


### Manual file editing
You will edit json files, so you need a text editor (Notepad++, VSCode...).

Exemple for `l5r5e.core-armors.json`, the current file look like :
```json
{
  "label": "Armors",
  "mapping": {
    "description": "system.description",
    "book_reference": "system.book_reference"
  },
  "entries": [{
    "id": "Ashigaru Armor",
    "name": "Ashigaru Armor",
    "description": "",
    "book_reference": "Core Rulebook p.239"
},{
...
```
> Never modify the `id`, this is the only reference to the original item, all others fields can be edited.
> Same for the `mapping` section, no modifications are required.

Usually you probably only need to change the `description`, but some have more (`types`, `category`, `grip1`...).
If you need to translate, you will modify all fields but `id`.

The previous exemple translated in French :
```json
{
  "label": "Armures",
  "mapping": {
    "description": "system.description",
    "book_reference": "system.book_reference"
  },
  "entries": [{
    "id": "Ashigaru Armor",
    "name": "Armure d'Ashigaru",
    "description": "Peu coûteuse, légère et facile à acquérir...",
    "book_reference": "Livre de Règles p.239"
},{
...
```

If you are copying from a PDF, use a [blank character cleaner and line break fixer](https://www.textfixer.com/tools/remove-line-breaks.php).

Now, to avoid issue with json format, and the ability to use system's symbols (ex `(op)`, `(ring)`) :
- Create a journal in foundry and fill it with description
- Open the associated file (`<FoundryData>/worlds/<world-name>/data/journal.db`)
- Copy it's content to the json file.

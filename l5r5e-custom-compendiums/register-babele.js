Hooks.once('babele.init', (babele) => {
    // En
    babele.register({
        module: 'l5r5e-custom-compendiums',
        lang: 'en',
        dir: 'compendiums/en'
    });

    // Fr
    // babele.register({
    //     module: 'l5r5e-custom-compendiums',
    //     lang: 'fr',
    //     dir: 'compendiums/fr'
    // });
});
